package ru.konovalov.tm;

import org.hibernate.boot.Metadata;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.konovalov.tm.api.IPropertyService;
import ru.konovalov.tm.dto.ProjectRecord;
import ru.konovalov.tm.dto.SessionRecord;
import ru.konovalov.tm.dto.TaskRecord;
import ru.konovalov.tm.dto.UserRecord;
import ru.konovalov.tm.exception.system.DatabaseInitException;
import ru.konovalov.tm.model.ProjectGraph;
import ru.konovalov.tm.model.SessionGraph;
import ru.konovalov.tm.model.TaskGraph;
import ru.konovalov.tm.model.UserGraph;
import ru.konovalov.tm.service.PropertyService;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import java.util.HashMap;
import java.util.Map;

public final class TestConnectionService implements ru.konovalov.tm.api.service.IConnectionService {

    @NotNull
    private final IPropertyService propertyService;

    @NotNull
    private final EntityManagerFactory entityManagerFactory;

    public TestConnectionService() {
        this.propertyService = new PropertyService();
        entityManagerFactory = factory();
    }

    @Override
    public @NotNull EntityManager getEntityManager() {
        return entityManagerFactory.createEntityManager();
    }

    @NotNull
    private EntityManagerFactory factory() {
        @Nullable final String driver = propertyService.getTestJdbcDriver();
        if (driver == null) throw new DatabaseInitException();
        @Nullable final String username = propertyService.getJdbcUser();
        if (username == null) throw new DatabaseInitException();
        @Nullable final String password = propertyService.getJdbcPassword();
        if (password == null) throw new DatabaseInitException();
        @Nullable final String url = propertyService.getTestJdbcUrl();
        if (url == null) throw new DatabaseInitException();
        @Nullable final String dialect = propertyService.getTestHibernateDialect();
        if (dialect == null) throw new DatabaseInitException();
        @Nullable final String auto = propertyService.getHibernateBM2DDLAuto();
        if (auto == null) throw new DatabaseInitException();
        @Nullable final String sqlShow = propertyService.getHibernateShowSql();
        if (sqlShow == null) throw new DatabaseInitException();

        @NotNull final Map<String, String> settings = new HashMap<>();
        settings.put(org.hibernate.cfg.Environment.DRIVER, driver);
        settings.put(org.hibernate.cfg.Environment.URL, url);
        settings.put(org.hibernate.cfg.Environment.USER, username);
        settings.put(org.hibernate.cfg.Environment.PASS, password);
        settings.put(org.hibernate.cfg.Environment.DIALECT, dialect);
        settings.put(org.hibernate.cfg.Environment.HBM2DDL_AUTO, auto);
        settings.put(org.hibernate.cfg.Environment.SHOW_SQL, sqlShow);

        @NotNull final StandardServiceRegistryBuilder registryBuilder = new StandardServiceRegistryBuilder();
        registryBuilder.applySettings(settings);
        @NotNull final StandardServiceRegistry registry = registryBuilder.build();
        @NotNull final MetadataSources sources = new MetadataSources(registry);

        sources.addAnnotatedClass(ProjectRecord.class);
        sources.addAnnotatedClass(TaskRecord.class);
        sources.addAnnotatedClass(SessionRecord.class);
        sources.addAnnotatedClass(UserRecord.class);

        sources.addAnnotatedClass(ProjectGraph.class);
        sources.addAnnotatedClass(TaskGraph.class);
        sources.addAnnotatedClass(SessionGraph.class);
        sources.addAnnotatedClass(UserGraph.class);

        @NotNull final Metadata metadata = sources.getMetadataBuilder().build();
        return metadata.getSessionFactoryBuilder().build();
    }

}
