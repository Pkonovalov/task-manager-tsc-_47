package ru.konovalov.tm.exception.empty;

import org.jetbrains.annotations.NotNull;
import ru.konovalov.tm.exception.AbstractException;

public class EmptyDomainException extends AbstractException {

    @NotNull
    public EmptyDomainException() {
        super("Error. Domain is empty");
    }

}