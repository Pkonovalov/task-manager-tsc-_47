package ru.konovalov.tm.exception.system;

import org.jetbrains.annotations.NotNull;
import ru.konovalov.tm.exception.AbstractException;

public class AccessDeniedException extends AbstractException {

    @NotNull
    public AccessDeniedException() {
        super("Error. Access denied.");
    }

}
