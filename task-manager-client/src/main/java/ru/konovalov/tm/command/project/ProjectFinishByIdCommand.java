package ru.konovalov.tm.command.project;

import org.jetbrains.annotations.Nullable;
import ru.konovalov.tm.command.ProjectAbstractCommand;
import ru.konovalov.tm.endpoint.ProjectDto;
import ru.konovalov.tm.exception.entity.ProjectNotFoundException;
import ru.konovalov.tm.util.TerminalUtil;

public class ProjectFinishByIdCommand extends ProjectAbstractCommand {
    @Override
    public String name() {
        return "project-finish-by-id";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Finish project by id.";
    }

    @Override
    public void execute() {
        System.out.println("Enter id");
        @Nullable final String id = TerminalUtil.nextLine();
        @Nullable final ProjectDto project = serviceLocator.getProjectEndpoint().finishProjectById(getSession(), id);
        if (project == null) throw new ProjectNotFoundException();
    }
}
