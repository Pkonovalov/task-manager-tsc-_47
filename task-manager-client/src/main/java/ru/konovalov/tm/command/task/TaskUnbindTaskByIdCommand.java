package ru.konovalov.tm.command.task;

import org.jetbrains.annotations.Nullable;
import ru.konovalov.tm.command.TaskAbstractCommand;
import ru.konovalov.tm.endpoint.TaskDto;
import ru.konovalov.tm.exception.entity.TaskNotFoundException;
import ru.konovalov.tm.util.TerminalUtil;

public class TaskUnbindTaskByIdCommand extends TaskAbstractCommand {
    @Override
    public String name() {
        return "task-unbind-by-id";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Unbind task from project.";
    }

    @Override
    public void execute() {
        System.out.println("Enter task id");
        @Nullable final String taskId = TerminalUtil.nextLine();
        @Nullable final TaskDto task = serviceLocator.getTaskEndpoint().findTaskById(getSession(), taskId);
        if (task == null) throw new TaskNotFoundException();
        serviceLocator.getTaskEndpoint().unbindTaskById(getSession(), taskId);
    }
}
