package ru.konovalov.tm.command.auth;

import ru.konovalov.tm.command.AuthAbstractCommand;

public class LogoutCommand extends AuthAbstractCommand {
    @Override
    public String name() {
        return "logout";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Logout from to application.";
    }

    @Override
    public void execute() {
        serviceLocator.getSessionEndpoint().close(getSession());
    }
}
