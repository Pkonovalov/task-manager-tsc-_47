package ru.konovalov.tm.command.system;

import org.jetbrains.annotations.NotNull;
import ru.konovalov.tm.command.AbstractCommand;

public class ArgumentsDisplayCommand extends AbstractCommand {
    @Override
    public String name() {
        return "arguments";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Show program arguments.";
    }

    @Override
    public void execute() {
        int index = 1;
        for (@NotNull final String arg : serviceLocator.getCommandService().getListCommandArg()) {
            System.out.println(index + ". " + arg);
            index++;
        }
    }
}
